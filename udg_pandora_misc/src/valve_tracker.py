#!/usr/bin/env python
"""Created on 22 October 2013
author Arnau
"""
# ROS imports
import roslib
roslib.load_manifest('udg_pandora_misc')
import rospy
#use to load the configuration function
from cola2_lib import cola2_ros_lib
#import the map to read the data of the filter
from pose_ekf_slam.msg import Map
#include message for the pose of the landmark
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped

from std_msgs.msg import Float64

from std_srvs.srv import Empty, EmptyResponse

import numpy as np

import tf

import threading


class valveTracker():
    """
    This class Track the different pose of the valves. The information of the
    different cameras is combined and filtered to obtain a regular and robust
    pose of each valve.
    """

    def __init__(self, name):
        """
        This method load the configuration and initialize the publishers,
        subscribers and tf broadcaster and publishers
        """
        self.name = name
        self.getconfig()
        # Created a broadcaster for the TK of each joint of the arm
        #self.br = tf.TransformBroadcaster()
        self.tflistener = tf.TransformListener()
        # Create the publisher
        # we use a list to allow to have a variable number of valves
        self.valve_publishers = []
        self.valve_poses = []
        self.valve_ori_pub = []
        self.valve_ori_cov = []
        self.last_update_tf = []
        self.valve_msg = []
        time = rospy.Time.now()
        self.last_update_ee_tf = time
        self.last_update_ee_p_tf = time

        self.enable_valve_ori = True

        self.lock = threading.Lock()
        self.lock_error = threading.Lock()
        self.panel_centre = Pose()
        
        
        self.assisted_valve_angle = [3.14, 3.14, 3.14, 3.14]
        

        #Linear Kalman Filter
        # oritentation parameters initial guess
        self.kf_valves_ori = np.zeros(self.num_valves)
        self.kf_q_error = np.ones(self.num_valves)*1e-5
        self.kf_p = np.ones(self.num_valves)

        # State transition matrix
        self.kf_a = np.ones(self.num_valves)
        # Control matrix
        self.kf_b = np.ones(self.num_valves)
        # The control vector will be 0

        #observations
        self.kf_r_error = np.ones(self.num_valves)*1e-5
        #observation matrix
        self.kf_h = np.ones(self.num_valves)

        #innovation
        self.kf_innov = np.zeros(self.num_valves)
        self.kf_innov_cov = np.zeros(self.num_valves)

        #predictions
        self.kf_p_hat = np.ones(self.num_valves)
        self.kf_valves_ori_hat = np.zeros(self.num_valves)


        #broadcaster for the valve pose
        self.tf_broadcaster = tf.TransformBroadcaster()
        for i in xrange(self.num_valves):
            pub_name = '/valve_tracker/valve'+str(i)
            self.valve_publishers.append(rospy.Publisher(
                pub_name, PoseWithCovarianceStamped))
            self.valve_poses.append([0, 0, 0])
            pub_name_ori = '/valve_tracker/valve_'+str(i)+'_ori'
            self.valve_ori_pub.append(rospy.Publisher(
                pub_name_ori, Float64))
            # pub_name_cov = '/valve_tracker/valve_ori_cov'+str(i)
            # self.valve_ori_cov.append(rospy.Publisher(
            #     pub_name_cov, Float64))
            self.valve_msg.append(PoseWithCovarianceStamped())
            self.last_update_tf.append(time)
        #Comented because is not used
        # self.pub_valve_landmark = rospy.Publisher(
        #     self.publisher_landmark, PoseWithCovarianceStamped)
        #subscrive to the Map where is the position of the center
        rospy.Subscriber("/pose_ekf_slam/map",
                         Map,
                         self.updatepanelpose,
                         queue_size = 1)
        rospy.Subscriber("/pose_ekf_slam/landmark_update/panel_centre",
                         PoseWithCovarianceStamped,
                         self.updatecovariance,
                         queue_size = 1)

        rospy.Subscriber("/pose_ekf_slam/landmark_update/panel_centre",
                         PoseWithCovarianceStamped,
                         self.updatecovariance,
                         queue_size = 1)

        # WORK AROUND
        rospy.Subscriber("/valve_tracker/valve_0_ass_ori",
                         Float64,
                         self.updateAngleValve0,
                         queue_size = 1)
        rospy.Subscriber("/valve_tracker/valve_1_ass_ori",
                         Float64,
                         self.updateAngleValve1,
                         queue_size = 1)
        rospy.Subscriber("/valve_tracker/valve_2_ass_ori",
                         Float64,
                         self.updateAngleValve2,
                         queue_size = 1)
        rospy.Subscriber("/valve_tracker/valve_3_ass_ori",
                         Float64,
                         self.updateAngleValve3,
                         queue_size = 1)


        self.enable_srv = rospy.Service(
            '/valve_tracker/enable_update_valve_orientation',
            Empty,
            self.enable_update_valve_srv)

        self.disable_srv = rospy.Service(
            '/valve_tracker/disable_update_valve_orientation',
            Empty,
            self.disable_update_valve_srv)

    def updateAngleValve0(self, data):
        self.assisted_valve_angle[0] = data.data        
    def updateAngleValve1(self, data):
        self.assisted_valve_angle[1] = data.data
    def updateAngleValve2(self, data):
        self.assisted_valve_angle[2] = data.data
    def updateAngleValve3(self, data):
        self.assisted_valve_angle[3] = data.data
        
        
        
    def getconfig(self):
        """
        This method load the configuration and initialize the publishers,
        subscribers and tf broadcaster and publishers
        """
        param_dict = {'period': '/valve_tracker/period',
                      'num_valves': '/valve_tracker/number_of_valves',
                      'name_valve_pose_ee': '/valve_tracker/name_valve_pose_ee',
                      'name_valve_ori_ee': '/valve_tracker/name_valve_ori_ee',
                      'landmark_id': '/valve_tracker/landmark_id',
                      'valve_dist_centre': '/valve_tracker/valve_dist_centre',
                      'valve_dist_centre_ee': '/valve_tracker/valve_dist_centre_ee',
                      'valve_id': '/valve_tracker/valve_id',
                      'publisher_landmark': '/valve_tracker/publisher_landmark'
                      }
        cola2_ros_lib.getRosParams(self, param_dict)

    def enable_update_valve_srv(self, req):
        self.enable_valve_ori = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()

    def disable_update_valve_srv(self, req):
        self.enable_valve_ori = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()

    def updatebumbleebetf(self):
        """
        This method check if there is a tf with the position of the valve pose
        provided using the bumblebee
        """
        for i in xrange(self.num_valves):
            try:
                # trans, rot = self.tflistener.lookupTransform(
                #     'world', ('valve'+str(i)),
                #     self.tflistener.getLatestCommonTime(
                #         'world', 'valve'+str(i)))
                trans, rot = self.tflistener.lookupTransform(
                    '/panel_centre', ('valve'+str(i)),
                    self.tflistener.getLatestCommonTime(
                        '/panel_centre', 'valve'+str(i)))
                #yaw
                #reading measurement
                time = self.tflistener.getLatestCommonTime(
                    '/panel_centre', 'valve'+str(i))
                if self.last_update_tf[i] < time:
                    self.last_update_tf[i] = time
                    euler = tf.transformations.euler_from_quaternion(
                        rot)
                    measurement = tf.transformations.euler_from_quaternion(
                        rot)[2]
                    #rospy.loginfo('Measurement valve ' + str(i) + ' : ' + str(measurement))
                    #rospy.loginfo('Euler valve ' + str(i) + ' : ' + str(euler))
                    #rospy.loginfo('Time ' + str(i) +
                    #              str(self.tflistener.getLatestCommonTime(
                    #                  'world', 'valve'+str(i))))
                    #--------------Observation step----------------
                    #innovation = measurement_vector -
                    #             self.H*predicted_state_estimate
                    self.kf_innov[i] = (measurement -
                                        self.kf_h[i] * self.kf_valves_ori_hat[i])
                    #innovation_covariance = self.H*predicted_prob_estimate*
                    #                        numpy.transpose(self.H) + self.R
                    # if i == 2 :
                    #     rospy.loginfo('***********************************************')
                    #     rospy.loginfo('Innovation 2        = ' + str(self.kf_innov[i]) )
                    #     rospy.loginfo('Predicted value 2   = ' + str(self.kf_valves_ori_hat[i]))
                    #     rospy.loginfo('Measurement valve 2 = ' + str(measurement))
                    #     rospy.loginfo('***********************************************')
                    self.lock_error.acquire()
                    try:
                        self.kf_innov_cov[i] = (self.kf_h[i] * self.kf_p_hat[i] *
                                                self.kf_h[i] + self.kf_r_error[i])
                    finally:
                        self.lock_error.release()
                else:
                    self.kf_innov[i] = 0.0
                    self.kf_innov_cov[i] = 0.0
            except tf.Exception:
                self.kf_innov[i] = 0.0
                self.kf_innov_cov[i] = 0.0
                # rospy.logerr(
                #     'Error reading the Transformation from world to EE')
        #rospy.loginfo('***********************************************')

    # def updatehandcameraposetf(self):
    #     """
    #     NOT USED
    #     This method check if there is a tf with the position of the valve pose
    #     provided using the camera in the end-effector
    #     """
    #     try:
    #         trans, rot = self.tflistener.lookupTransform(
    #             'base_arm', self.name_valve_pose_ee,
    #             self.tflistener.getLatestCommonTime(
    #                 'base_arm', self.name_valve_pose_ee))
    #         time = self.tflistener.getLatestCommonTime(
    #             'base_arm', self.name_valve_pose_ee)
    #         if self.last_update_ee_p_tf < time:
    #             self.last_update_ee_p_tf = time
    #             pose_valve = PoseWithCovarianceStamped()
    #             pose_valve.pose.pose.position.x = trans[0]
    #             pose_valve.pose.pose.position.y = trans[1]
    #             pose_valve.pose.pose.position.z = trans[2]
    #             pose_valve.pose.pose.orientation.x = rot[0]
    #             pose_valve.pose.pose.orientation.y = rot[1]
    #             pose_valve.pose.pose.orientation.z = rot[2]
    #             pose_valve.pose.pose.orientation.w = rot[3]
    #             cov = 0.035*np.eye(6)
    #             cov_p = (
    #                 (((1.2*np.linalg.norm(np.asarray(trans)))**2)*0.03)**2)
    #             cov[0, 0] = cov_p
    #             cov[1, 1] = cov_p
    #             cov[2, 2] = cov_p
    #             rospy.loginfo('Trans ' + str(trans))
    #             rospy.loginfo('Norm de la trans ' + str(np.linalg.norm(np.asarray(trans))))
    #             rospy.loginfo('Covariance ' + str(cov_p))
    #             rospy.loginfo('Matrix ' + str(cov) )
    #             pose_valve.pose.covariance = cov.flatten().tolist()
    #             # pose_valve.pose.covariance[0] = 0.0
    #             # pose_valve.pose.covariance[7] = 0.003
    #             # pose_valve.pose.covariance[14] = 0.003
    #             # pose_valve.pose.covariance[21] = 0.035
    #             # pose_valve.pose.covariance[28] = 0.035
    #             # pose_valve.pose.covariance[35] = 0.035
    #             pose_valve.header.stamp = rospy.Time.now()
    #             pose_valve.header.frame_id = 'base_arm'
    #             self.pub_valve_landmark.publish(pose_valve)
    #     except tf.Exception:
    #         pass
    #         # rospy.logerr(
    #         #     'Error reading the Tranformation from world to EE')

    def updatehandcameraoritf(self):
        """
        This method check if there is a tf with the position and orientation of
        the valve pose provided using the camera in hand
        """
        try:
            trans, rot = self.tflistener.lookupTransform(
                'base_arm', self.name_valve_ori_ee,
                self.tflistener.getLatestCommonTime(
                    'base_arm', self.name_valve_ori_ee))
            #yaw
            #reading measurement
            time = self.tflistener.getLatestCommonTime(
                '/base_arm', self.name_valve_ori_ee)
            i = self.valve_id
            if self.last_update_ee_tf < time:
                self.last_update_ee_tf = time
                measurement = tf.transformations.euler_from_quaternion(
                    rot)[2]
                #--------------Observation step----------------
                #innovation = measurement_vector -
                #             self.H*predicted_state_estimate
                self.kf_innov[i] = (measurement -
                                    self.kf_h[i] * self.kf_valves_ori_hat[i])
                #innovation_covariance = self.H*predicted_prob_estimate*
                #                        numpy.transpose(self.H) + self.R
                self.lock_error.acquire()
                try:
                    # TODO: Cacula la invertesa.
                    self.kf_innov_cov[i] = (self.kf_h[i] * self.kf_p_hat[i] *
                                            self.kf_h[i] + self.kf_r_error[i])
                finally:
                    self.lock_error.release()
            else:
                self.kf_innov[i] = 0.0
                self.kf_innov_cov[i] = 0.0
        except tf.Exception:
            pass
            # rospy.logerr(
            #     'Error reading the Tranformation from world to EE')

    def updatepanelpose(self, landmarkmap):
        """
        This method recive the data filtered from the ekf_map and publish the
        position for the valve
        """
        self.lock.acquire()
        try:
            for mark in landmarkmap.landmark:
                #rospy.loginfo(' Lanmark ' +str(mark.landmark_id) + ' Config ' + str(self.landmark_id))
                if self.landmark_id == mark.landmark_id:
                    self.panel_centre = mark.pose.pose
                    #rospy.loginfo('Panel pose ' + str(mark.pose.pose))
                    #Create the Transformation matrix
                    trans_mat = tf.transformations.quaternion_matrix(
                        [self.panel_centre.orientation.x,
                         self.panel_centre.orientation.y,
                         self.panel_centre.orientation.z,
                         self.panel_centre.orientation.w])
                    trans_mat[0, 3] = self.panel_centre.position.x
                    trans_mat[1, 3] = self.panel_centre.position.y
                    trans_mat[2, 3] = self.panel_centre.position.z

                    #invert the Matrix
                    # inv_mat = np.zeros([4, 4])
                    # inv_mat[3, 3] = 1.0
                    # inv_mat[0:3, 0:3] = np.transpose(trans_mat[0:3, 0:3])
                    # inv_mat[0:3, 3] = np.dot((-1*inv_mat[0:3, 0:3]),
                    #                          trans_mat[0:3, 3])

                    # rospy.loginfo('Panel Centre ' +
                    #               str(self.panel_centre.position.x) +
                    #               str(self.panel_centre.position.y) +
                    #               str(self.panel_centre.position.z) )
                    for i in xrange(self.num_valves):
                        valve_pose = np.ones(4)
                        valve_pose[0:3] = self.valve_dist_centre[i]
                        self.valve_poses[i] = np.dot(trans_mat, valve_pose)
                        v_pose = PoseWithCovarianceStamped()
                        v_pose.pose.pose.position.x = self.valve_poses[i][0]
                        v_pose.pose.pose.position.y = self.valve_poses[i][1]
                        v_pose.pose.pose.position.z = self.valve_poses[i][2]
                        v_pose.pose.pose.orientation = self.panel_centre.orientation
                        v_pose.pose.covariance = mark.pose.covariance
                        v_pose.header.stamp = rospy.Time.now()
                        self.valve_msg[i] = v_pose
                        #self.valve_publishers[i].publish(v_pose)
                        # rospy.loginfo('Valve ' + str(i) + ' : '
                        #               + str(self.valve_poses[i]))
        finally:
            self.lock.release()

    def updatecovariance(self, msg):
        """
        This method recive the data published by the visual detector and obtain
        the covariance of the position to use in the KF
        """
        self.lock_error.acquire()
        try:
            self.kf_r_error = np.ones(self.num_valves) * msg.pose.covariance[0]
        finally:
            self.lock_error.release()

    def predictpose(self):
        """
        This method recive the data filtered from the ekf_map and publish the
        position for the valve
        """
        #We supose the valve is not moving so the control is 0
        #predicted_state_estimate = self.A * self.current_state_estimate
        #                           + self.B * control_vector
        self.kf_valves_ori_hat = (self.kf_a * self.kf_valves_ori +
                                  self.kf_b * 0.0)
        # predicted_prob_estimate = (self.A * self.current_prob_estimate) *
        #                            numpy.transpose(self.A) + self.Q
        self.kf_p_hat = (self.kf_a * self.kf_p)*self.kf_a + self.kf_q_error

    def updatekf(self):
        """
        update the value of the kalman filter value for each filter
        """
        #-----------------------------Update step-------------------------------
        #kalman_gain = predicted_prob_estimate * numpy.transpose(self.H)
        #              * numpy.linalg.inv(innovation_covariance)
        kalman_gain = np.zeros(self.num_valves)
        for i in xrange(self.num_valves):
            if self.kf_innov_cov[i] == 0.0:
                self.kf_valves_ori[i] = self.kf_valves_ori_hat[i]
                self.kf_p[i] = self.kf_p_hat[i]
            else:
                kalman_gain[i] = (self.kf_p_hat[i] * self.kf_h[i] *
                                  (1/self.kf_innov_cov[i]))
                # if i == 2 :
                #     rospy.loginfo('x_hat ' + str(i) + ' ' + str(self.kf_valves_ori_hat[i]))
                #     rospy.loginfo('innov_cov ' + str(i) + ' ' + str(1/self.kf_innov_cov[i]))
                self.kf_valves_ori[i] = (self.kf_valves_ori_hat[i] +
                                         kalman_gain[i] * self.kf_innov[i])
                self.kf_p[i] = (1-kalman_gain[i]*self.kf_h[i])*self.kf_p_hat[i]
                #self.valve_ori_pub[i].publish(self.kf_valves_ori[i])
            # self.valve_ori_cov[i].publish(self.kf_p[i])
        #rospy.loginfo('********************************************')

    def updatekfhand(self):
        """
        update the value of the kalman filter value of the end_effector
        """
        #-----------------------------Update step-------------------------------
        #kalman_gain = predicted_prob_estimate * numpy.transpose(self.H)
        #              * numpy.linalg.inv(innovation_covariance)
        kalman_gain = 0.0
        i = self.valve_id
        if self.kf_innov_cov[i] == 0.0:
            self.kf_valves_ori[i] = self.kf_valves_ori_hat[i]
            self.kf_p[i] = self.kf_p_hat[i]
        else:
            kalman_gain = (self.kf_p_hat[i] * self.kf_h[i] *
                           (1/self.kf_innov_cov[i]))
            self.kf_valves_ori[i] = (self.kf_valves_ori_hat[i] +
                                     kalman_gain * self.kf_innov[i])
            self.kf_p[i] = (1-kalman_gain*self.kf_h[i])*self.kf_p_hat[i]

    def publish(self):
        """
        Publish the data updated in the kalman filter
        """
        for i in xrange(self.num_valves):
            self.lock.acquire()
            try:
                quat = np.zeros(4)
                quat[0] = self.valve_msg[i].pose.pose.orientation.x
                quat[1] = self.valve_msg[i].pose.pose.orientation.y
                quat[2] = self.valve_msg[i].pose.pose.orientation.z
                quat[3] = self.valve_msg[i].pose.pose.orientation.w
                eul = tf.transformations.euler_from_quaternion(np.asarray(quat))
                #replacing the yaw
                #eul[2] = self.kf_valves_ori[i]
                # quat = tf.transformations.quaternion_from_euler(
                #     eul[0], eul[1], eul[2]+self.kf_valves_ori[i])
                #rospy.loginfo('Euler values ' + str(eul))
                #rospy.loginfo('Euler inc ' + str(self.kf_valves_ori[i]))
                
                
                #WORK AROUND
                angle = self.discretize_valve_angle(self.kf_valves_ori[i])
                #angle = self.assisted_valve_angle[i]
                
                
                
                rot_matrix = tf.transformations.euler_matrix(
                    0.0, 0.0, angle)
                panel_matrix = tf.transformations.quaternion_matrix([
                    self.valve_msg[i].pose.pose.orientation.x,
                    self.valve_msg[i].pose.pose.orientation.y,
                    self.valve_msg[i].pose.pose.orientation.z,
                    self.valve_msg[i].pose.pose.orientation.w])
                #res = np.dot(rot_matrix, panel_matrix)
                res = np.dot( panel_matrix, rot_matrix )
                quat = tf.transformations.quaternion_from_matrix(
                     res)
                valve_msg = PoseWithCovarianceStamped()
                valve_msg.pose.pose.position = self.valve_msg[i].pose.pose.position
                #rospy.loginfo('Valve ' +str(i) + ' Pose ' + str(self.valve_msg[i].pose.pose.position))
                valve_msg.pose.pose.orientation.x = quat[0]
                valve_msg.pose.pose.orientation.y = quat[1]
                valve_msg.pose.pose.orientation.z = quat[2]
                valve_msg.pose.pose.orientation.w = quat[3]
                cov = np.asarray(self.valve_msg[i].pose.covariance[:])
                cov[35] = self.kf_p[i]
                valve_msg.pose.covariance = cov
                self.valve_msg[i].pose.covariance = cov
                valve_msg.header.stamp = rospy.Time.now()
                self.valve_publishers[i].publish(valve_msg)
                self.tf_broadcaster.sendTransform(
                    (valve_msg.pose.pose.position.x,
                     valve_msg.pose.pose.position.y,
                     valve_msg.pose.pose.position.z),
                    (quat[0],quat[1],quat[2],quat[3]),
                    rospy.Time.now(),
                    "valve_"+str(i)+"_tracker",
                    "world")

                self.valve_ori_pub[i].publish(self.discretize_valve_angle(self.kf_valves_ori[i]))
                # self.valve_ori_cov[i].publish(self.kf_p[i])
            finally:
                self.lock.release()
        #rospy.loginfo('****************************')
    def run(self):
        """
        This is the main loop where the prediction of the filter is done and the
        pose and orientation of each valve is published with a regular frequency
        """
        while not rospy.is_shutdown():
            self.predictpose()
            if self.enable_valve_ori:
                self.updatebumbleebetf()
                self.updatekf()
            #self.updatehandcameraoritf()
            #self.updatekfhand()
            self.publish()
            #self.updatehandcameraposetf()
            rospy.sleep(self.period)

    def set_default_parameters(self):
        """
        This function load by default param configurations
        """
        pass

    def wrap_angle_zero_pi(self, angle):
        """
        This function gets the positive orientation of the valve (only 1st
        and 2n)
        """
        if 0.0 < angle <= np.pi:
            return angle
        else:
            return (np.pi + angle)
            
    def angle_valve(self, angle):
        if np.pi >=angle and angle >= (np.pi/2.0):
            return angle
        elif -np.pi <=angle and angle <= (3*(-np.pi/4.0)):
            return np.pi
        elif (3*(-np.pi/4.0)) <= angle and angle <= (-np.pi/2.0):
            return np.pi/2.0
        elif (-np.pi/2.0) <= angle and angle <= 0.0:
            return (np.pi + angle)
        elif 0.0 <= angle and angle <= (np.pi/4.0):
            return np.pi
        elif (np.pi/2.0) >= angle and angle >=(np.pi/4.0):
            return np.pi/2.0
        else:
            rospy.logerr('Wrong Angle')

    def discretize_valve_angle(self, angle):
        if ((angle < 2.1 and angle > 0.78) or (angle < -1.05 and angle > -2.35)):
            return 1.57
        elif ((angle >= 2.1 and angle <= 2.62) or (angle <= -0.524 and angle >= -1.05)):
            return 2.35
        else:
            return 3.14
                

if __name__ == '__main__':
    try:
        import subprocess
        # Load ROS parameters
        config_file_list = roslib.packages.find_resource("udg_pandora_misc",
            "valve_tracker.yaml")
        if len(config_file_list):
            config_file = config_file_list[0]
            subprocess.call(["rosparam", "load", config_file])
        else:
            print "Could not locate visual_detector.yaml, using defaults"
            #set_default_parameters()
        #rospy.sleep(5.0)
        rospy.init_node('valve_tracker')
        VALVETRACKER = valveTracker(rospy.get_name())
        VALVETRACKER.run()
    except rospy.ROSInterruptException:
        rospy.logerr('The  has stopped unexpectedly')
